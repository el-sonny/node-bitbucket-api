var should = require("should");
var bitbucket = require('../index');
var goodOptions = require('./helper').credentials;
var repoData = require('./helper').repository;

describe('BitBucket.user()', function() {
  var client;
  before(function (done) {
    client = bitbucket.createClient(goodOptions);
    done();
  });

  describe(".follows()", function () {
    it('should return a list of repositories the user follows', function (done) {
      client.user().follows(function (err, result) {
        Array.isArray(result).should.be.ok;
        result[0].should.have.property('slug');
        done(err);
      });
    });
  });

  describe(".get()", function () {
    it('should return the user and repository data for the logged in user', function (done) {
      client.user().get(function (err, result) {
        result.should.have.property("repositories");
        result.should.have.property("user");
        done(err);
      });
    });
  });

  describe(".privileges()", function () {
    it('should return the user privileges', function (done) {
      client.user().privileges(function (err, result) {
        result.should.have.property("teams");
        done(err);
      });
    });
  });

  describe(".update()", function () {
    it('should update the user', function (done) {
      var firstName = "Hernan D.";
      client.user().update({"first_name": firstName}, function (err, result) {
        result.first_name.should.eql(firstName);
        done(err);
      });
    });
  });

  describe(".repositories().getAll()", function () {
    it('should return the user repositories', function (done) {
      client.user().repositories().getAll(function (err, result) {
        Array.isArray(result).should.be.ok;
        done(err);
      });
    });
  });

  describe(".repositories().following()", function () {
    it('should return details on the repositories that is following', function (done) {
      client.user().repositories().following(function (err, result) {
        result.should.have.property('updated');
        result.should.have.property('viewed');
        done(err);
      });
    });
  });

  describe(".repositories().dashboard()", function () {
    it('should return the dashboard', function (done) {
      client.user().repositories().dashboard(function (err, result) {
        Array.isArray(result).should.be.ok;
        done(err);
      });
    });
  });

  after(function(done) {
      client.user().update({"first_name": "Hernan"}, function (err, result) {
        done(err);
      });
  });

});
